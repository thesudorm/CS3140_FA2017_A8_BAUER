var called = false;
var isAnimated = false;
var timerArry = [];
var k = 0;
var DIRECTIONS = ["up", "down", "right", "left"];

window.onload = function() {

    $("#add").bind('click', function(){
        // for(var i = 0; i < 1000; i++){
            addCircle();
        // }
    })

    $("#reset").bind('click', function(){
        reset();
    })

    $("#change").bind('click', function(){
        color()});

    $("#animate").bind('click', function(){
        if (isAnimated == false){
            isAnimated = true;
            
            $(".circle").each(function(){
                timerArry[k] = upDown($(this));
                k++;
            })

            k = 0;
        }
        else{
            isAnimated = false;

            $(".circle").each(function(){
                clearInterval(timerArry[k]);
                k++
            })

            k = 0;
        }
        
    });

    bindCircles();

}; 

//Green gave me the idea to just update the css positions with set interval
//Two functions, one updates the top css the other left
//Maybe randomly assign each circle one of these functions

 function upDown (obj) {
    var direction = randomDirection();

    timer = setInterval(function(){
                if (direction == "down"){
                    obj.css("top", "+=3px");
                    if (obj.css("top").replace('px', '') > $("#main").height() - 50) {
                        direction = "up";
                        oneColor(obj);
                    }
                } else if (direction == "up") {
                    obj.css("top", "-=3px");
                    if (obj.css("top").replace('px', '') < 0){
                        direction = "down";
                        oneColor(obj);
                    }
                } else if (direction == "right") {
                    obj.css("left", "+=3px");
                    if (obj.css("left").replace('px', '') > $("#main").width() - 50){
                        direction = "left";
                        oneColor(obj);
                    }
                } else if (direction == "left"){
                    obj.css("left", "-=3px");
                    if (obj.css("left").replace('px', '') < 0){
                        direction = "right";
                        oneColor(obj);
                    }
                }
            }, 10);

    return timer;
 }

 function randomDirection(){
     return DIRECTIONS[Math.floor(Math.random()*DIRECTIONS.length)];
 }

//This function will ensure that every circel has the fnctioanlity bound to
//every circle after reset or ad circle buton is pressed
function bindCircles() {
    $(".circle").bind('click', function(){
        foreground($(this));
    })

    $(".circle").bind('dblclick', function(){
        deleteCircle($(this));
    })

    // $(".animated").toggleClass("animated");
    
}

 function makeCircles() {
    //Create 50 divs with id circle
    for (i = 0; i < 50; i++) {
        addCircle();
    }
}

//All of the functions that deal with color

//Changes the color of every circle
function color() {
    var list = $(".circle").each(function(){
        $(this).css("background-color", getRandomColor());
    });
}

//Changes one circle color
function oneColor(obj) {
    obj.css("background-color", getRandomColor());
}

function getRandomColor(){
    var letters = "0123456789abcdef";
    var result = "#";

    for(var i=0; i<6; i++){
        result += letters.charAt(parseInt(Math.random() * letters.length));
    }

    return result;
}

//Random Coordinates
function xCoordinate() {
    var x = $("#main").width() - 48;

    return Math.floor((Math.random() * x) + 1);
}

function yCoordinate() {
    var y = $("#main").height() - 48;

    return Math.floor((Math.random() * y) + 1);
}

function placeAll() {
    $(".circle").each(function() {
        placeCircle($(this));
    });
}

function placeCircle(obj){
    obj.css("top", getRandomCoordinate());
    obj.css("left", getRandomCoordinate());
}

//BUTTONS/////////////////////////////////////////////////////////////
function addCircle() {
    $("#main").append(function(){
        return $('<div />', {
            "class": 'circle animated',
            "style": "background-color: " + getRandomColor() + "; top: " + yCoordinate() + "px; left: " + xCoordinate() + "px;"
        })
    })

    bindCircles();
}

function reset(){
    $("#main").empty();
    makeCircles();
    bindCircles();
}

//Circle Functionality
//TO bring to foreground, need to append the current node and then delete it

//Ask green if append actually moves the element or copies it.
function foreground(obj){
    $("#main").append(obj);
}

function deleteCircle(obj){
    obj.remove();
}

//Questions for Friday
//1) How to stop svg from over lapping?...
//  Fill the divs with a background color and shape$ it
//2)Can I just refresh the page instead of doing a whol reset function?...
//3)Proper way to add elements in javascript?