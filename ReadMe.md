# Assignment 8 - HTML, CSS, JS

This was an assignment in the Web Dev class I took in the fall of 2017. The page simple draws a bunch of circles on the page and you are able to animate them using the provided buttons. 

# Requirements

There are none for this assignment. Simply clone and open up index.html in any browser.

# Author

Joseph Bauer
bauerj@bgsu.edu
